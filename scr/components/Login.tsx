import React, {Component, Fragment} from "react";
import {View, Text, StyleSheet, Button} from "react-native";
import {GoogleSignin, GoogleSigninButton, statusCodes} from 'react-native-google-signin';
import Home from "../screens/home/Home";

interface Props {
}

interface State {
    isSigninInProgress: any;
    userInfo: any;
    loggedIn: boolean;
    user: any;
    error: any;
}

export default class Login extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            isSigninInProgress: null,
            userInfo: null,
            loggedIn: false,
            user: null,
            error: null
        };
    }

    componentDidMount(): void {
        GoogleSignin.configure({
            scopes: ['profile, email'],
            webClientId: '999315050420-4ssmhvbucb8kgmrnb83sagid25rbci1j.apps.googleusercontent.com',
            offlineAccess: true,
            forceConsentPrompt: true,
        });
    }

    async _signIn() {
        try {
            const userInfo = await GoogleSignin.signIn();
            this.setState({userInfo: userInfo, loggedIn: true});
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (f.e. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    async signOut() {
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
            this.setState({user: null, loggedIn: false}); // Remember to remove the user from your app's state as well
        } catch (error) {
            console.error(error);
        }
    };


    async getCurrentUserInfo() {
        try {
            const userInfo = await GoogleSignin.signInSilently();
            this.setState({userInfo});
        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_REQUIRED) {
                // user has not signed in yet
                this.setState({loggedIn: false});
            } else {
                // some other error
                this.setState({loggedIn: false});
            }
        }
    };

    renderHome() {
        if (this.state.loggedIn) {
            return (
                <Home
                    loggedIn={this.state.loggedIn}
                    logout={this.signOut}>
                </Home>
            );
        }
    };

    renderLogin() {
        if (!this.state.loggedIn) {
            return (
                <View style={styles.sectionContainer}>
                    <Text>{JSON.stringify(this.state.error)}</Text>
                    <GoogleSigninButton
                        style={{width: 192, height: 48}}
                        size={GoogleSigninButton.Size.Wide}
                        color={GoogleSigninButton.Color.Dark}
                        onPress={this._signIn}/>
                </View>
            );
        }
    };

    render() {
        return (
            <Fragment>
                <View style={styles.body}>
                    {this.renderLogin()}
                    {this.renderHome()}
                </View>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    body: {
        backgroundColor: '#FFFFFF',
    },
    sectionContainer: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
