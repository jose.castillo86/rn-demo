import React, { Component } from "react";
import { createStackNavigator, createBottomTabNavigator } from "react-navigation";
import { Icon } from "native-base";
import Home from "../screens/home/Home";
import Post from "../screens/post/Post";

const HomeStack = createStackNavigator({ Home: Home }, { headerMode: "none" });

const PostStack = createStackNavigator({ Post: Post }, { headerMode: "none" });

const bottomTabNavigatorConfig = {
    tabBarOptions: {
        activeTintColor: "white",
        inactiveTintColor: "gray",
        style: { backgroundColor: "cyan" },
        showIcon: true,
        showLabel: true,
    },
};

export const BottomTabNavigator = createBottomTabNavigator(
    {
        Home: {
            screen: HomeStack,
            navigationOptions: {
                tabBarIcon: ({ focused, horizontal, tintColor }) => (
                    <Icon name="home" style={{ color: tintColor }} type={"AntDesign"} />
                ),
            },
        },
        Post: {
            screen: PostStack,
            navigationOptions: {
                tabBarIcon: ({ focused, horizontal, tintColor }) => (
                    <Icon name="camera" style={{ color: tintColor }} type={"Feather"} />
                ),
            },
        },
    },
    bottomTabNavigatorConfig
);
