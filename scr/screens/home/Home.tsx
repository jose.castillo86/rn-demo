import React, { Component, Fragment } from "react";
import { View, Text, Button, StyleSheet } from "react-native";
import { NavigationScreenProp, NavigationState } from "react-navigation";
import Login from "../../components/Login";

interface Props {
    // navigation: NavigationScreenProp<NavigationState & any>;
    loggedIn: boolean;
    logout: Function;
}
interface State {}

export default class Home extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <Fragment>
                <View style={styles.sectionContainer}>
                    <View style={styles.buttonContainer}>
                        {!this.props.loggedIn && <Login />}
                        {this.props.loggedIn && <Button onPress={this.props.logout}
                                                        title="Logout "
                                                        color="#841584">
                        </Button>}
                    </View>
                </View>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        paddingHorizontal: 24,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    sectionContainer: {
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
    },
});
